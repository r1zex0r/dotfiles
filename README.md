# Dotfiles

My dotfiles inspired by pwnbox theme by HackTheBox.

![Showcase Image](https://gitlab.com/r1zex0r/dotfiles/-/raw/main/images/1.png)

## Dependencies

- GNU Stow (for managing dotfiles)
- tmux
- xsel
- ohmyzsh
- terminator
- mononoki Nerd Font Mono
- vim

## Getting started

1. Install ohmyzsh
2. Delete `.zshrc`
3. Clone into this repository into `~/.dotfiles`
4. cd into that
5. cd into stow
6. Delete `~/.config/xfce4`
7. `stow -nvt ~ *`

## Additional info

`xinput set-prop 'Logitech G102 LIGHTSYNC Gaming Mouse' 'libinput Accel Speed' -0.7`
