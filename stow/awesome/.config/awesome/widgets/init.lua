local awful = require('awful')
local wibox = require('wibox')
local build_widget = require('widgets.build_widget')
beautiful = require('beautiful')

local widgets = {}

-- From Files
widgets.vol = require('widgets.vol')

-- Separators
widgets.space = wibox.widget.textbox('<span>  </span>')
widgets.seperator = wibox.widget.textbox(string.format(' <span color="%s">|</span>  ', 'gray'))
widgets.seperator.font = "Roboto Medium 12"

-- Textclock
local textclock_text =wibox.widget.textclock(" %d %b %a %I:%M %p")
widgets.textclock = build_widget:new(textclock_text, "", '#ffffff').widget

local month_calendar = awful.widget.calendar_popup.month()
month_calendar:attach( widgets.textclock, 'tc' )

return widgets
