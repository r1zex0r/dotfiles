set rnu
set nu

lua require('plugins')
lua require('lualine-config')
lua require('gruvbox-config')

" Tab setup
filetype plugin indent on
" show existing tab with 4 spaces width
set tabstop=4
" when indenting with '>', use 4 spaces width
set shiftwidth=4
" On pressing tab, insert 4 spaces
set expandtab
